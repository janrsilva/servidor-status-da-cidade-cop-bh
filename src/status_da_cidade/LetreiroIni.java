/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package status_da_cidade;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.File;
import org.ini4j.Ini;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.server.ServerNotActiveException;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author Janderson
 */
public class LetreiroIni extends UnicastRemoteObject implements IniInterface {

    /**
     * FINALS
     */
    private final String billboard      = "cop-bh-billboard";
    private final String end            = "end";
    private final String end_config_cop = "config.cop";

    /**
     * ATRIBUTOS
     */
    private Ini ini;
    private String endereco;
    private String mensagem = "";

    public LetreiroIni() throws RemoteException {
        super();
    }

    public Ini getIni() {
        return ini;
    }

    public void setIni(Ini ini) {
        this.ini = ini;
    }

//    public void exibeMensagem() throws InterruptedException, RemoteException {
//        new Thread() {
//            @Override
//            public void run(){
//                final int X = 0;
//                final int Y = 0;
//                Toolkit tk = Toolkit.getDefaultToolkit();
//                Dimension d = tk.getScreenSize();
//                int width = d.width;
//                int height = 100;
//                JLabel jLabel = new JLabel();
//                LetreiroMarquee letreiro = null;
//                try {
//                    letreiro = new LetreiroMarquee(X, Y, width, height, getPropriedade("note-0","txt"));
//                } catch (RemoteException ex) {
//                    Logger.getLogger(LetreiroIni.class.getName()).log(Level.SEVERE, null, ex);
//                }
//                try {
//                    System.out.println(getPropriedade("note-0","txt"));
//                } catch (RemoteException ex) {
//                    Logger.getLogger(LetreiroIni.class.getName()).log(Level.SEVERE, null, ex);
//                }
//                letreiro.setFont(new Font("Arial", Font.PLAIN, 80));
//                JFrame jFrame = new JFrame("COP-BH");
//                jFrame.getContentPane().add(letreiro);
//                jFrame.setSize(width, height);
//                jFrame.setUndecorated(true);
//                jFrame.setVisible(true);
//                try {
//                    letreiro.start();
//                } catch (InterruptedException ex) {
//                    Logger.getLogger(LetreiroIni.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }.start();
//    }
    
    /**
     * Sub classe resposável por definir um tipo de arquivo personalizado, que
     * será localizado para configuração do caminho percorrido até o
     * arquivoletreiro._ini.
     *
     */
    class TipoArquivoCOP extends javax.swing.filechooser.FileFilter {

        /**
         * Método obrigatório determina a leitura de apenas diretório ou
         * arquivos de extensão "._ini".
         *
         * @param file
         * @return boolean
         */
        @Override
        public boolean accept(File file) {
            return file.isDirectory() || file.getAbsolutePath().endsWith(".ini");
        }

        /**
         * Método obrigatório determina descrição para o tipo de arquivo
         * "._ini".
         *
         * @return String
         */
        @Override
        public String getDescription() {
            return "Arquivo de Configuração COP-BH (*.ini)";
        }
    }

    /**
     * Método obrigatório que realiza a leitura do arquivo _ini, se arquivo ini
     * não tem endereço definido, chama metodo set_endereco_arquivo_ini que
     * define endereço.
     *
     * @return boolean
     * @throws RemoteException envio geral de exception remota.
     */
    @Override
    public boolean leArquivoIni() throws RemoteException {
        
        if (this.get_endereco_ini() != null) {
            System.out.println("Método get_endereco_ini retorno != null");
            try {
                FileInputStream file = new FileInputStream(this.get_endereco_ini());
                Ini _ini = new Ini(file);
                this.ini = _ini;
            } catch (FileNotFoundException ex) {
                System.out.println("Erro no Servidor! Método leArquivoIni \nMensagem: " + ex.getMessage() + "\nClass: " + ex.getClass());
                return false;
            } catch (IOException ex) {
                System.out.println("Erro no Servidor! Método leArquivoIni \nMensagem: " + ex.getMessage() + "\nClass: " + ex.getClass());
                return false;
            }
        } else {
            System.out.println("Método get_endereco_ini retorno = null");
            try {
                String end = this.set_endereco_arquivo_ini();
                if (end == null) {
                    System.out.println("Método set_endereco_arquivo_ini retornou = null");
                    return false;
                }
                FileInputStream file = new FileInputStream(end);
                Ini _ini = null;
                try {
                    _ini = new Ini(file);
                } catch (IOException ex) {
                    Logger.getLogger(LetreiroIni.class.getName()).log(Level.SEVERE, null, ex);
                }
                this.ini = _ini;
            } catch (FileNotFoundException ex) {
                System.out.println("Erro no Servidor! Método leArquivoIni \nMensagem: " + ex.getMessage() + "\nClass: " + ex.getClass());
                return false;
            }
        }
        mensagem = this.ini.get("note-0", "txt");
        new ShowMessage().start();
        return true;
    }

    /**
     * Método obrigatório responsável por encontrar e retornar o endereço do
     * arquivo ini.
     *
     * @return String
     * @throws RemoteException envio geral de exception remota.
     */
    @Override
    public String set_endereco_arquivo_ini() throws RemoteException {

        Ini ini_cop = new Ini();
        File config_cop = new File(this.end_config_cop);

        if (!config_cop.exists()) {
            JPrompt.printPane("Bem vindo!", "É necessário localizar o arquivo Letreiro.ini. \n\nClique em OK para localizar o arquivo.");
            JFileChooser jFileChooser = new JFileChooser();
            jFileChooser.setFileFilter(new TipoArquivoCOP());
            int returnVal = jFileChooser.showOpenDialog(null);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = jFileChooser.getSelectedFile();
                this.endereco = file.getPath();
                System.out.println("Método set_endereco_arquivo_ini jFileChooser retornou o endereço " + this.endereco);
                try {
                    ini_cop.put(this.billboard, this.end, this.endereco);
                    FileWriter file_cop = new FileWriter(config_cop);
                    ini_cop.store(file_cop);
                    file_cop.close();
                    System.out.println("Arquivo " + this.end_config_cop + " salvo com sucesso!");
                } catch (IOException ex) {
                    System.out.println("Erro no Servidor! Método set_endereco_arquivo_ini \nMensagem: " + ex.getMessage() + "\nCausa: " + ex.getCause());
                }
                return this.endereco;
            } else {
                System.out.println("Acesso ao arquivo cancelado pelo usuário.");
                return null;
            }
        } else {
            try {
                ini_cop = new Ini(config_cop);
                Ini.Section section = ini_cop.get(this.billboard);
                this.endereco = section.get(this.end);
                System.out.println("Método set_endereco_arquivo_ini endereço adquirido é " + this.endereco);
            } catch (IOException ex) {
                config_cop.delete();
                System.out.println("Método set_endereco_arquivo_ini arquivo" + this.end_config_cop + " excluido!");
                System.out.println("Erro no Servidor! Método set_endereco_arquivo_ini \nMensagem: " + ex.getMessage() + "\nCausa: " + ex.getCause());
            }
        }
        return this.endereco;
    }

    /**
     * Método obrigatório responsável por retornar endereço do arquivo
     * letreiro.ini.
     *
     * @return String
     * @throws RemoteException
     */
    @Override
    public String get_endereco_ini() throws RemoteException {
        String _endereco = null;
        File config_cop = new File(this.end_config_cop);
        if (config_cop.exists()) {
            Ini ini;
            try {
                ini = new Ini(config_cop);
            } catch (IOException ex) {
                config_cop.delete();
                System.out.println(ex.getLocalizedMessage());
                return null;
            }
            try {
                Ini.Section section = ini.get(this.billboard);
                if (section.containsKey(this.end)) {
                    this.endereco = section.get(this.end);
                    _endereco = this.endereco;
                    BufferedReader leitura = new BufferedReader(new FileReader(_endereco));
                    Ini _ini = new Ini(leitura);
                } else {
                    config_cop.delete();
                    return null;
                }
            } catch (NullPointerException | IOException ex) {
                config_cop.delete();
                System.out.println("Erro no Servidor! Método get_endereco_ini \nMensagem: " + ex.getMessage());
                System.out.println("O arquivo \"" + this.end_config_cop + "\" possui configurações incorretas e será excluído! ");
                return null;
            }
        }
        return _endereco;
    }

    /**
     * Método obrigatório responsável por gravar arquivo de configuração para
     * encontrar arquivo letreiro.ini.
     *
     * @throws RemoteException
     */
    @Override
    public void setConfigCOP() throws RemoteException {
        String _endereco = null;
        JFileChooser jFileChooser = new JFileChooser("../");
        jFileChooser.setFileFilter(new TipoArquivoCOP());
        int returnVal = jFileChooser.showOpenDialog(null);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = jFileChooser.getSelectedFile();
            this.endereco = file.getPath();
            Ini ini_cop = new Ini();
            ini_cop.put(this.billboard, this.end, this.endereco);
            try {
                FileWriter file_cop = new FileWriter(this.end_config_cop, false);
                ini_cop.store(file_cop);
                file_cop.close();
            } catch (IOException ex) {
                System.out.println("Erro no Servidor! \nMensagem: " + ex.getMessage() + "\nCausa: " + ex.getCause());
            }
        } else {
            System.out.println("Acesso ao arquivo cancelado pelo usuário.");
        }
    }

    /**
     * Método responsável por gravar o arquivo _ini no diretório do servidor.
     *
     * @return retorna o resultado da operação.
     * @throws RemoteException envio geral de exception remota.
     */
    @Override
    public Boolean gravaArquivoIni() throws RemoteException {
        
        try {
            FileWriter escrita = new FileWriter(this.get_endereco_ini(), false);
            this.ini.store(escrita);
            escrita.close();
        } catch (IOException ex) {
            System.out.println("Erro no Servidor! \nMensagem: " + ex.getMessage() + "\nCausa: " + ex.getCause());
        }
        System.out.println("Status gravado com sucesso!");
        return true;
    }

    @Override
    public Boolean verificaArqConfigCOP() throws RemoteException {

        File config_cop = new File(this.end_config_cop);
        BufferedReader leitura;
        if (!config_cop.exists()) {
            System.out.println("Arquivo config.cop não existe!");
            return false;
        } else {
            try {
                Ini config_ini = new Ini(config_cop);
                Ini.Section section = config_ini.get(this.billboard);
                String _endereco = section.get(this.end);
                leitura = new BufferedReader(new FileReader(_endereco));
                Ini _ini = new Ini(leitura);
            } catch (FileNotFoundException | NullPointerException ex) {
                System.out.println("Erro no Servidor! \nMensagem: " + ex.getMessage() + "\nClass: " + ex.getClass() + "\nCausa: " + ex.getCause());
                config_cop.delete();
                return false;
            } catch (RemoteException ex) {
                System.out.println("Erro no Servidor! \nMensagem: " + ex.getMessage() + "\nClass: " + ex.getClass() + "\nCausa: " + ex.getCause());
                return false;
            } catch (IOException ex) {
                System.out.println("Erro no Servidor! \nMensagem: " + ex.getMessage() + "\nClass: " + ex.getClass() + "\nCausa: " + ex.getCause());
                config_cop.delete();
                return false;
            }
            System.out.println("Arquivo config.cop existe!");
            return true;
        }
    }

    /**
     * Método obrigatório responsável por recuperar string do arquivo
     * letreiro.ini de acordo com a sessão e chave. Ex: note_0, txt, color...
     *
     * @param sessao
     * @param chave
     * @return String
     * @throws RemoteException
     */
    @Override
    public String getPropriedade(String sessao, String chave) throws RemoteException {
        Ini.Section section = this.ini.get(sessao);
        return section.get(chave);
    }

    /**
     * Método obrigatório responsável por setar as propridades definidas pelo
     * cliente no objeto ini contido no servidor.
     *
     * @param sessao
     * @param chave
     * @param valor
     * @throws RemoteException
     */
    @Override
    public void setPropriedade(String sessao, String chave, String valor) throws RemoteException {
        this.ini.put(sessao, chave, valor);
    }

    /**
     * Método obrigatório responsável por verificar se a comunicação com o
     * servidor está on-line.
     *
     * @param mensagem
     * @return String
     * @throws RemoteException
     */
    @Override
    public String hello_servidor(String mensagem) throws RemoteException {
        try {
            return "Olá Cliente!!! Seu IP é " + UnicastRemoteObject.getClientHost()
                    + "\nA mensagem recebida foi \""
                    + mensagem + "\"";
        } catch (ServerNotActiveException ex) {
            System.out.println("Erro no Servidor! \nMensagem: " + ex.getMessage() + "\nClass: " + ex.getClass());
        }
        return null;
    }
    
    public class ShowMessage extends Thread{
        int cont = 50;
        @Override
        public void run(){
            while (true){
                final int X = 0;
                final int Y = 0;
                Toolkit tk = Toolkit.getDefaultToolkit();
                Dimension d = tk.getScreenSize();
                int width = d.width;
                int height = 100;
                JLabel jLabel = new JLabel();
                LetreiroMarquee letreiro = null;
                letreiro = new LetreiroMarquee(X, Y, width, height, getIni().get("note-0", "txt"));
                
                System.out.println(getIni().get("note-0", "txt"));
                
                letreiro.setFont(new Font("Arial", Font.PLAIN, 80));
                JFrame jFrame = new JFrame("COP-BH");
                jFrame.getContentPane().add(letreiro);
                jFrame.setSize(width, height);
                jFrame.setUndecorated(true);
                jFrame.setVisible(true);
                try {
                    letreiro.start();
                } catch (InterruptedException ex) {
                    Logger.getLogger(LetreiroIni.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}