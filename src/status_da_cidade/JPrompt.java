package status_da_cidade;
import java.util.HashMap;
import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 * Classe para leitura e escrita de dados pelo teclado via prompt e JOptionPane.
 *
 * @author Professor Moisés Henrique Ramos Pereira
 * @version 4.1
 */
public class JPrompt {
    
    // Declaração de classe interna.
    private static class Simbolo {
        
        public HashMap<String,Integer> tabela = new HashMap<>();
        
        public Simbolo() {
            tabela.put("x", JOptionPane.ERROR_MESSAGE); 
            tabela.put("i", JOptionPane.INFORMATION_MESSAGE); 
            tabela.put("?", JOptionPane.QUESTION_MESSAGE);
            tabela.put("!", JOptionPane.WARNING_MESSAGE);
            tabela.put("", JOptionPane.PLAIN_MESSAGE);
            tabela.put(null, JOptionPane.PLAIN_MESSAGE);
        }
        
        public Integer getIcone(String tipo) {
            Integer codigoIcone = tabela.get(tipo);
            return codigoIcone == null ? JOptionPane.PLAIN_MESSAGE : codigoIcone;
        }
    }
    
    // Declaração dos atributos.
    private static final String erroSemDado = "Nenhum dado informado!";
    private static final String erroNaoReal = "Nenhum número real informado!";
    private static final String erroNaoIntg = "Nenhum número inteiro informado!";
    
    private static final Simbolo tabela = new Simbolo();
    private static final Scanner prompt = new Scanner(System.in);
    
    // Declaração dos métodos.
    
    /*****************************************************************
     * Métodos para escrita no prompt, terminal ou janelas popup.    *
     *****************************************************************/
    
    /**
     * @param mensagem
     */
    public static void print(String mensagem) {
        System.out.print(mensagem);
    }
    
    /**
     * @param mensagem
     */
    public static void printLine(String mensagem) {
        System.out.println(mensagem);
    }
    
    /**
     * @param titulo
     * @param mensagem
     * @param icone
     */
    public static void printPane(String titulo, String mensagem, String icone) {
        JOptionPane.showMessageDialog(null, mensagem, titulo, tabela.getIcone(icone.toLowerCase()));
    }

    /**
     * @param titulo
     * @param mensagem
     */
    public static void printPane(String titulo, String mensagem) {
        printPane(titulo, mensagem, "i");
    }
    
    /**
     * @param mensagem
     */
    public static void printPane(String mensagem) {
        printPane("Informação", mensagem, "i");
    }
    
    /**
     * @param titulo
     * @param mensagem
     * @return Valor inteiro referente a opçao escolhida.
     */
    public static Integer printConf(String titulo, String mensagem) {
        return JOptionPane.showConfirmDialog(null, mensagem, titulo, JOptionPane.YES_NO_OPTION);
    }
    
    /*****************************************************************
     * Métodos para leitura a partir do teclado via prompt ou popup. *
     *****************************************************************/
    
    /**
     * @param mensagem
     * @return 
     */
    public static Integer readInt(String mensagem) {
        String entrada;
        Boolean existeErro;
        Integer numeroInt = 0;

        do {
            existeErro = false;
            try {
                System.out.print(mensagem);
                entrada = prompt.nextLine();
                numeroInt = Integer.parseInt(entrada.trim());
            } catch (NumberFormatException e) {
                printLine(erroNaoIntg);
                existeErro = true;
            }
        } while (existeErro);
        return numeroInt;
    }
    
    /**
     * @param titulo
     * @param mensagem
     * @param icone
     * @return 
     */
    public static Integer readIntPane(String titulo, String mensagem, String icone) {
        String entrada;
        Boolean existeErro;
        Integer numeroInt = Integer.MIN_VALUE;

        do {
            existeErro = false;
            try {
                entrada = JOptionPane.showInputDialog(null, mensagem, titulo, tabela.getIcone(icone.toLowerCase()));
                if (entrada != null) {
                    numeroInt = Integer.parseInt(entrada.trim());
                }
            } catch (NumberFormatException e) {
                printPane("ERRO", erroNaoIntg, "x");
                existeErro = true;
            } catch (NullPointerException e) {
                printPane("ERRO", erroSemDado, "x");
                existeErro = true;
            }
        } while (existeErro);
        return numeroInt;
    }
    
    /**
     * @param titulo
     * @param mensagem
     * @return Integer
     */
    public static Integer readIntPane(String titulo, String mensagem) {
        return readIntPane(titulo, mensagem, "?");
    }
    
    /**
     * @param mensagem
     * @return Integer
     */
    public static Integer readIntPane(String mensagem) {
        return readIntPane("Leitura de Int", mensagem, "?");
    }
    
    /**
     * @param mensagem
     * @return Float
     */
    public static Float readFloat(String mensagem) {
        String entrada;
        Boolean existeErro;
        Float numeroFloat = Float.NaN;

        do {
            existeErro = false;
            try {
                print(mensagem);
                entrada = prompt.nextLine().trim();
                numeroFloat = Float.parseFloat(entrada);
            } catch (NumberFormatException e) {
                printLine(erroNaoReal);
                existeErro = true;
            }
        } while (existeErro);
        return numeroFloat;
    }
    
    /**
     * @param titulo
     * @param mensagem
     * @param icone
     * @return Float
     */
    public static Float readFloatPane(String titulo, String mensagem, String icone) {
        String entrada;
        Boolean existeErro;
        Float numeroFloat = Float.NaN;

        do {
            existeErro = false;
            try {
                entrada = JOptionPane.showInputDialog(null, mensagem, titulo, tabela.getIcone(icone.toLowerCase()));
                if (entrada != null) {
                    numeroFloat = Float.parseFloat(entrada.trim());
                }
            } catch (NumberFormatException e) {
                printPane("ERRO", erroNaoReal, "x");
                existeErro = true;
            } catch (NullPointerException e) {
                printPane("ERRO", erroSemDado, "x");
                existeErro = true;
            }
        } while (existeErro);
        return numeroFloat;
    }
    
    /**
     * @param titulo
     * @param mensagem
     * @return Float
     */
    public static Float readFloatPane(String titulo, String mensagem) {
        return readFloatPane(titulo, mensagem, "?");
    }
    
    /**
     * @param mensagem
     * @return Float
     */
    public static Float readFloatPane(String mensagem) {
        return readFloatPane("Leitura de Float", mensagem, "?");
    }
    
    /**
     * @param mensagem
     * @return Double
     */
    public static Double readDouble(String mensagem) {
        String entrada;
        Boolean existeErro;
        Double numeroDouble = Double.NaN;

        do {
            existeErro = false;
            try {
                print(mensagem);
                entrada = prompt.nextLine();
                numeroDouble = Double.parseDouble(entrada.trim());
            } catch (NumberFormatException e) {
                printLine(erroNaoReal);
                existeErro = true;
            }
        } while (existeErro);
        return numeroDouble;
    }
    
    /**
     * @param titulo
     * @param mensagem
     * @param icone
     * @return Double
     */
    public static Double readDoublePane(String titulo, String mensagem, String icone) {
        String entrada;
        Boolean existeErro;
        Double numeroDouble = Double.NaN;

        do {
            existeErro = false;
            try {
                entrada = JOptionPane.showInputDialog(null, mensagem, titulo, tabela.getIcone(icone.toLowerCase()));
                if (entrada != null) {
                    numeroDouble = Double.parseDouble(entrada.trim());
                }
            } catch (NumberFormatException e) {
                printPane("ERRO", erroNaoReal, "x");
                existeErro = true;
            } catch (NullPointerException e) {
                printPane("ERRO", erroSemDado, "x");
                existeErro = true;
            }
        } while (existeErro);
        return numeroDouble;
    }
    
    /**
     * @param mensagem
     * @return Double
     */
    public static Double readDoublePane(String mensagem) {
        return readDoublePane("Leitura de Double", mensagem, "?");
    }
    
    /**
     * @param titulo
     * @param mensagem
     * @return Double
     */
    public static Double readDoublePane(String titulo, String mensagem) {
        return readDoublePane(titulo, mensagem, "?");
    }
    
    public static String readString(String mensagem) {
        print(mensagem);
        return prompt.nextLine().trim();
    }
    
    /**
     * @param titulo
     * @param mensagem
     * @param icone
     * @return String
     */
    public static String readStringPane(String titulo, String mensagem, String icone) {
        String entrada = new String();
        Boolean existeErro;

        do {
            existeErro = false;
            try {
                entrada = JOptionPane.showInputDialog(null, mensagem, titulo, tabela.getIcone(icone.toLowerCase()));
            } catch (NullPointerException e) {
                printPane("ERRO", erroSemDado, "x");
                existeErro = true;
            }
        } while (existeErro);
        return entrada;
    }
    
    /**
     * @param titulo
     * @param mensagem
     * @return String
     */
    public static String readStringPane(String titulo, String mensagem) {
        return readStringPane(titulo, mensagem, "?");
    }
    
    /**
     * @param mensagem
     * @return String
     */
    public static String readStringPane(String mensagem) {
        return readStringPane("Leitura de String", mensagem, "?");
    }
    
    public static Character readChar(String mensagem) {
        print(mensagem);
        return prompt.next().charAt(0);
    }
    
    /**
     * @param titulo
     * @param mensagem
     * @param icone
     * @return Character
     */
    public static Character readCharPane(String titulo, String mensagem, String icone) {
        String entrada;
        Boolean existeErro;
        Character caractere = '\0';

        do {
            existeErro = false;
            try {
                entrada = JOptionPane.showInputDialog(null, mensagem, titulo, tabela.getIcone(icone.toLowerCase()));
                if (entrada != null) {
                    caractere = entrada.trim().charAt(0);
                }
            } catch (NullPointerException e) {
                printPane("ERRO", erroSemDado, "x");
                existeErro = true;
            } catch (StringIndexOutOfBoundsException e) {
                caractere = ' ';
            }
        } while (existeErro);
        return caractere;
    }
    
    /**
     * @param mensagem
     * @return Character
     */
    public static Character readCharPane(String mensagem) {
        return readCharPane("Leitura de Char", mensagem, "?");
    }
}
