/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package status_da_cidade;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import javax.swing.JPanel;

/**
 * https://www.youtube.com/watch?v=HKX2sAQL13g
 * @author 
 */
public class LetreiroMarquee extends JPanel {
    private int x;
    private int y;
    private int width;
    private int height;
    private int px_font_size;
    private String mensagem;
    private int strWidth;

    public LetreiroMarquee(int x, int y, int width, int height, String mensagem) {
        
        this.x = width;
        this.y = height;
        this.width = width;
        this.height = height;
        this.mensagem = mensagem;
        setSize(this.width, this.height);
    }

    @Override
    public void paint(Graphics g){
        Font font = new Font("Arial", Font.PLAIN, 80);
        
        g.setColor(Color.blue);
        g.fillRect(0, 0, this.width, this.height);
        g.setColor(Color.white);
        g.setFont(font);
        strWidth = getFontMetrics(getFont()).stringWidth(mensagem);
        g.drawString(mensagem, x, y);
        System.out.println(x +" "+ y);
        
        px_font_size = getFontMetrics(getFont()).getHeight();
        System.out.println(strWidth);
    }
    
    public void start() throws InterruptedException{
        while(true){
            x = 0;
            while(x >= strWidth*-1){
               x--;
               y = getHeight()/2 + 25;
               repaint();
               Thread.sleep(7);
            }
            this.y = height;
        }
    }
}