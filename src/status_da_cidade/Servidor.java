/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package status_da_cidade;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.InetAddress;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import org.ini4j.Ini;

/**
 *
 * @author Janderson
 */
public class Servidor {

    /**
     * FINALS para arquivo ini.
     */
    private String end_porta_cop    = "porta.cop";
    private String porta_sessao     = "cop-bh-porta";
    private String porta_key        = "porta";
    /**
     * FINALS para rmi.
     */
    private int porta_padrao        = 20161;
    private Registry registry;
    private String ip;
    private String porta;
    private String nome;

    /**
     * Método construtor que instância um objeto e chama método que define porta
     * de uso.
     */
    public Servidor() {
        define_porta(porta_padrao);
        try {
            InetAddress _ip = InetAddress.getLocalHost();
            this.ip = _ip.getHostAddress();
            this.nome = _ip.getHostName();
            System.out.println(_ip.getHostAddress() + ":" + this.porta);
            System.out.println(_ip.getHostName());
        } catch (java.net.UnknownHostException ex) {
            System.out.println("Erro no Servidor! \nMensagem: " + ex.getMessage() + "\nCausa: " + ex.getCause());
        }
    }

    public Registry getRegistry() {
        return registry;
    }

    public void setRegistry(Registry registry) {
        this.registry = registry;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPorta() {
        return porta;
    }

    public void setPorta(String porta) {
        this.porta = porta;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * Método responsável por definir a porta a ser usada para disponibilizar
     * objeto remoto na rede.
     *
     * @param porta_padrao
     * @return boolean
     */
    private boolean define_porta(int porta_padrao) {
        try {
            if (!get_porta_file()) {
                this.registry = LocateRegistry.createRegistry(porta_padrao);
                this.porta = String.valueOf(porta_padrao);
            } else {
                this.registry = LocateRegistry.createRegistry(Integer.parseInt(this.porta));
            }
            return true;
        } catch (RemoteException ex) {
            System.out.println("Porta já está sendo usada!");
            JPrompt.printPane("Porta já está sendo usada! \nMensagem: " + ex.getMessage() + "\n\nClique em OK para definir uma nova porta!");
            int _porta = JPrompt.readIntPane("Definindo Porta", "Digite o númeto da porta:");

            if (set_porta_file(_porta)) {
                while (!define_porta(_porta)) {
                    JPrompt.printPane("Porta já está sendo usada! \nMensagem: " + ex.getMessage() + "\n\nClique em OK para definir uma nova porta!");
                    _porta = JPrompt.readIntPane("Definindo Porta", "Digite o númeto da porta:");
                    define_porta(_porta);
                }
            }
            return false;
        }
    }

    /**
     * Método responsável por retornar se existe um arquivo com configuração de
     * porta a ser usada e colocar essa informação na instância de objeto.
     *
     * @return boolean
     */
    private boolean get_porta_file() {
        File porta_cop = new File(this.end_porta_cop);
        if (!porta_cop.exists()) {
            System.out.println("O arquivo \""
                    + this.end_porta_cop
                    + "\" não existe, a porta padrão "
                    + this.porta_padrao
                    + " será usada!");
            return false;
        } else {
            try {
                Ini ini = new Ini(porta_cop);
                Ini.Section section = ini.get(this.porta_sessao);
                if (ini.containsKey(this.porta_key)) {
                    String _porta = section.get(this.porta_key);
                    /**
                     * Se der erro o valor string não continha apenas números e
                     * disparado uma exection que exclui o arquivo porta.cop
                     */
                    int testa_int = Integer.parseInt(_porta);
                    this.porta = String.valueOf(testa_int);
                } else {
                    porta_cop.delete();
                    return false;
                }
            } catch (IOException ex) {
                porta_cop.delete();
                System.out.println("O arquivo \""
                        + this.end_porta_cop
                        + "\" não possui configurações válidas e será excluido!");
                System.out.println("Erro! \nMensagem: " + ex.getMessage());
            }
        }
        return true;
    }

    /**
     * Método responsável por gravar arquivo com porta a ser usada para
     * disponibilizar objeto remoto na rede.
     *
     * @param porta
     * @return boolean
     */
    public boolean set_porta_file(int porta) {
        Ini porta_ini = new Ini();
        porta_ini.put(this.porta_sessao, this.porta_key, String.valueOf(porta));
        try {
            try (FileWriter porta_cop = new FileWriter(this.end_porta_cop, false)) {
                porta_ini.store(porta_cop);
            }
            System.out.println("Arquivo " + this.end_porta_cop + " gravado com sucesso!");
        } catch (IOException ex) {
            System.out.println("Erro! \nMensagem: " + ex.getMessage() + "\nClass: " + ex.getClass());
            return false;
        }
        return true;
    }

    /**
     * Método responsável por disponibilizar o objeto remoto na rede.
     *
     * @throws RemoteException
     * @throws AlreadyBoundException
     */
    public void iniciaServico() throws RemoteException, AlreadyBoundException {
        try {
            LetreiroIni letreiro = new LetreiroIni();
            letreiro.leArquivoIni();
            this.registry.rebind("StatusCidade", letreiro);
            System.out.println("Serviço iniciado com sucesso!");
        } catch (RemoteException e) {
            System.out.println("Erro no Servidor! \nMensagem: " + e.getMessage() + "\nCausa: " + e.getCause());
        }
    }

    /**
     * Método responsável por interromper a exportação do objeto remoto na rede.
     *
     * @throws RemoteException
     * @throws AlreadyBoundException
     */
    public void interrompeServico() throws RemoteException, AlreadyBoundException {
        if (this.registry != null) {
            UnicastRemoteObject.unexportObject(this.registry, true);
            System.out.println("Serviço interrompido com sucesso!");
        }
    }
}
