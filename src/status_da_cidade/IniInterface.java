/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package status_da_cidade;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *
 * @author Janderson
 */
public interface IniInterface extends Remote {

    /**
     * Definição de método obrigatório para leitura do arquivo ini.
     *
     * @return boolean
     * @throws RemoteException envio geral de exception remota.
     */
    public boolean leArquivoIni() throws RemoteException;

    /**
     * Definição de método obrigatório responsável por encontrar e retornar o
     * endereço do arquivo ini.
     *
     * @return String endereço captado para o arquivo ini.
     * @throws RemoteException RemoteException envio geral de exception remota.
     */
    public String set_endereco_arquivo_ini() throws RemoteException;

    /**
     * Definição de método obrigatório responsável por gravar o arquivo ini no
     * diretório do servidor.
     *
     * @return Boolean confirmando se o arquivo foi salvo. ini.
     * @throws RemoteException RemoteException envio geral de exception remota.
     */
    public Boolean gravaArquivoIni() throws RemoteException;

    /**
     * Definição de método obrigatório responsável por validar se existe o
     * arquivo de configuração config.cop.
     *
     * @return Boolean confirmando se existe o arquivo.
     * @throws RemoteException
     */
    public Boolean verificaArqConfigCOP() throws RemoteException;

    /**
     * Definição de método obrigatório responsável por recuperar string do
     * arquivo letreiro.ini de acordo com a sessão e chave. Ex: note_0, txt,
     * color...
     *
     * @param sessao
     * @param chave
     * @return String
     * @throws RemoteException
     */
    public String getPropriedade(String sessao, String chave) throws RemoteException;

    /**
     * Definição de método obrigatório responsável por setar as propridades
     * definidas pelo cliente no objeto ini contido no servidor.
     *
     * @param sessao
     * @param chave
     * @param valor
     * @throws RemoteException
     */
    public void setPropriedade(String sessao, String chave, String valor) throws RemoteException;

    /**
     * Definição de método obrigatório responsável por gravar arquivo de
     * configuração para encontrar arquivo letreiro.ini.
     *
     * @throws RemoteException
     */
    public void setConfigCOP() throws RemoteException;

    /**
     * Definição de método obrigatório responsável por retornar endereço do
     * arquivo letreiro.ini.
     *
     * @return String
     * @throws RemoteException
     */
    public String get_endereco_ini() throws RemoteException;

    /**
     * Definição de método obrigatório responsável por verificar se a
     * comunicação com o servidor está on-line.
     *
     * @param mensagem
     * @return String
     * @throws RemoteException
     */
    public String hello_servidor(String mensagem) throws RemoteException;
}
